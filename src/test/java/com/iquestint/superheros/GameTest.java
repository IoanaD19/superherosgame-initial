package com.iquestint.superheros;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class GameTest {
    private EngageGame game;

    @Test
    public void shouldEngage2CharactersInBattle() {
        // GIVEN
        GameCharacter popeye = new GameCharacter("Popeye", "Sailor", 15, 1, GameCharacter.CHARACTER_TYPE.HERO);
        GameCharacter bluto = new GameCharacter("Bluto", "Big and bad", 8, 3, GameCharacter.CHARACTER_TYPE.MONSTER);
        Weapon harpoon = new Weapon("harpoon", "For hunting whales.", 2, 1);
        popeye.pickUpWeapon(harpoon);

        game = new EngageGame(popeye, bluto);

        // WHEN
        game.charactersInBattle();

        // THEN
        Assert.assertThat(popeye.getCurrentHealth(), CoreMatchers.equalTo(99));
        Assert.assertThat(bluto.getCurrentHealth(), CoreMatchers.equalTo(97));
        Assert.assertThat(harpoon.isActive(), CoreMatchers.equalTo(false));
    }

    @Test
    public void shouldExecuteHealthUpdatesAndWeaponReloadWhenTurnIsOver() {
        // GIVEN
        GameCharacter popeye = new GameCharacter("Popeye", "Sailor", 15, 1, GameCharacter.CHARACTER_TYPE.HERO);
        GameCharacter bluto = new GameCharacter("Bluto", "Big and bad", 8, 3, GameCharacter.CHARACTER_TYPE.MONSTER);
        Weapon harpoon = new Weapon("harpoon", "For hunting whales.", 2, 1);
        popeye.pickUpWeapon(harpoon);
        game = new EngageGame(popeye, bluto);
        game.charactersInBattle();

        // WHEN
        game.newTurn();

        // THEN
        Assert.assertThat(popeye.getCurrentHealth(), CoreMatchers.equalTo(100));
        Assert.assertThat(bluto.getCurrentHealth(), CoreMatchers.equalTo(98));
        Assert.assertThat(harpoon.isActive(), CoreMatchers.equalTo(true));
    }
}
