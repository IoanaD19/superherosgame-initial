package com.iquestint.superheros;

import com.iquestint.superheros.GameCharacter.CHARACTER_TYPE;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class GameCharacterTest {
    private GameCharacter hero;

    @Before
    public void setUp() {
        hero = new GameCharacter("Hero1", "Hero1 description", 30, 10, CHARACTER_TYPE.HERO);
    }

    @Test
    public void shouldHaveMaxHealthWhenCreated() {
        // WHEN + THEN
        assertThat(hero.getCurrentHealth(), is(100));
    }

    @Test
    public void shouldHaveDefaultWeaponWhenCreated() {
        // WHEN
        Weapon w = hero.getBestWeapon();
        // THEN
        assertThat(w, CoreMatchers.notNullValue());
        assertThat(w.getDamage(), is(1));
    }

    @Test
    public void shouldHaveInitialProtectionAccordingToDefensiveSkills() {
        // WHEN + THEN
        assertThat(hero.getDefenseProtection(), is(1));
    }

    @Test
    public void protectionShouldIncreaseWHenCharmaIncreases() {
        // WHEN
        hero.increaseExperience(40);
        // THEN
        assertThat(hero.getDefenseProtection(), is(5));
    }

    @Test
    public void protectionShouldIncreaseWhenAddingAShield() {
        // WHEN
        try {
            hero.pickUpShield(new Shield("shield1", "shield1 description", 20));
        } catch (NotAValidOperation e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // THEN
        assertThat(hero.getDefenseProtection(), is(21));
    }
}
