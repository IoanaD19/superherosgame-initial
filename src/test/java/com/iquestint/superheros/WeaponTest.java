package com.iquestint.superheros;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class WeaponTest {
    @Test
    public void shouldBeDisabledAfterOneUseIfUsedAndNeedsReloading() {
        // GIVEN
        Weapon w = new Weapon("dagger", "dagger description", 1, 2);
        // WHEN
        w.useWeapon();
        // THEN
        assertThat(w.isActive(), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfUsedWhenDisabled() {
        // GIVEN
        Weapon w = new Weapon("dagger", "dagger description", 1, 2);
        // WHEN
        w.useWeapon();
        // THEN
        w.useWeapon();
    }

    @Test
    public void shouldBeEnabledWhenBuilt() {
        // GIVEN + WHEN
        Weapon w = new Weapon("dagger", "dagger description", 1, 2);

        // THEN
        assertThat(w.isActive(), is(true));
    }
}
