package com.iquestint.superheros;

public class GameItem {
    private String name;
    private String description;

    public GameItem(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
