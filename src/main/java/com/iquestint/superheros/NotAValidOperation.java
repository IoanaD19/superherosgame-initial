package com.iquestint.superheros;

public class NotAValidOperation extends Exception {

    public NotAValidOperation(String string) {
        super(string);
    }
}
