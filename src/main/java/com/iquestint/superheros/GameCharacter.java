package com.iquestint.superheros;

import java.util.LinkedList;
import java.util.List;


public class GameCharacter {
    public enum CHARACTER_TYPE {
        HERO, MONSTER
    }

    // This only applies to heros!!!
    public int experience = 0;
    // Only heros have shields!
    public List<Shield> shields = new LinkedList<Shield>();

    private static final Weapon DEFAULT_WEAPON = new Weapon("Fist", "Used when no other weapon available", 1, 0);

    public CHARACTER_TYPE type;
    protected String name;
    protected String description;
    public List<Weapon> weapons = new LinkedList<Weapon>();

    // skills
    protected int attackSkills;
    protected int defenseSkills;

    // health
    protected int currentHealth;

    public GameCharacter(String name, String description, int attackSkills, int defenseSkills, CHARACTER_TYPE type) {
        this.name = name;
        this.description = description;
        this.attackSkills = attackSkills;
        this.defenseSkills = defenseSkills;
        this.currentHealth = 100;
        this.weapons.add(DEFAULT_WEAPON);
        this.type = type;
    }

    public int getDefenseProtection() {
        switch (type) {
            case HERO: {
                int shieldProtection = 0;
                for (Shield s : this.shields) {
                    shieldProtection += s.getProtection();
                }
                return shieldProtection + (this.defenseSkills + this.experience) / 10;
            }
            case MONSTER: {
                return this.defenseSkills / 5;
            }
        }
        throw new IllegalArgumentException("This should never happen!");
    }

    public void increaseExperience(int amount) {
        if (type != CHARACTER_TYPE.HERO) {
            throw new IllegalArgumentException("This should never happen!");
        }
        experience += amount;
    }

    protected int getAttackDamage(Weapon w) {
        switch (type) {
            case HERO: {
                return w.getDamage() + (getAttackSkills() + getExperience()) / 10;
            }
            case MONSTER: {
                return w.getDamage() + getAttackSkills() / 10;
            }
        }
        throw new IllegalArgumentException("This should never happen!");
    }

    public int getAttackSkills() {
        return attackSkills;
    }

    public int getExperience() {
        return this.experience;
    }

    protected int getHealingAmount() {
        if (this.experience != 0) {
            // this is HERO!
//			System.out.println(this.type);
            return 1 + experience / 10;
        }
        return 1;
    }

    public boolean isAlive() {
        return currentHealth >= 0;
    }

    public void newTurn() {
        this.currentHealth = this.currentHealth + getHealingAmount();
        if (currentHealth > 100) {
            this.currentHealth = 100;
        }
        for (Weapon w : this.weapons) {
            w.newTurn();
        }
    }

    public Weapon getBestWeapon() {
        Weapon bestWeapon = null;
        int maxDamage = 0;
        for (Weapon w : this.weapons) {
            if (w.isActive() && (w.getDamage() > maxDamage)) {
                maxDamage = w.getDamage();
                bestWeapon = w;
            }
        }
        return bestWeapon;
    }

    public String getName() {
        return name;
    }

    public void pickUpWeapon(Weapon weapon) {
        this.weapons.add(weapon);
    }

    public void pickUpShield(Shield weapon) throws NotAValidOperation {
        if (this.type != CHARACTER_TYPE.HERO) {
            throw new NotAValidOperation("This should never happen");
        }
        this.shields.add(weapon);
    }

    public int getCurrentHealth() {
        return this.currentHealth;
    }
}
