package com.iquestint.superheros;


public class Weapon extends GameItem {
    private int damage;
    private int turnsToReload;
    private int turnsTillActive;

    public Weapon(String name, String description, int damage, int turnsToReload) {
        super(name, description);
        this.damage = damage;
        this.turnsToReload = turnsToReload;
        this.turnsTillActive = 0;
    }

    public int getDamage() {
        return damage;
    }

    public boolean isActive() {
        return turnsTillActive == 0;
    }

    public void useWeapon() {
        if (!isActive()) {
            throw new IllegalArgumentException("Can't use an weapon that's not loaded.");
        }
        turnsTillActive = turnsToReload;
    }

    public void newTurn() {
        if (turnsTillActive > 0) {
            turnsTillActive--;
        }
    }
}
