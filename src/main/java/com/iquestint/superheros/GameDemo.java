package com.iquestint.superheros;

import com.iquestint.superheros.GameCharacter.CHARACTER_TYPE;


public class GameDemo {

    public static void main(String[] args) {
        GameCharacter hero = new GameCharacter("Brave Ballador", "Paladin protecting the Urn of King Terenas", 15, 10,
                CHARACTER_TYPE.HERO);
        GameCharacter monster = new GameCharacter("Ar-dular", "Black dragon living in Black Forrest", 25, 10,
                CHARACTER_TYPE.MONSTER);

        Weapon sword = new Weapon("Knight sword", "Double edge blade.", 4, 0);
        Weapon electricStorm = new Weapon("Electric storm", "High amperage discharge", 15, 3);

        Shield elfArmour = new Shield("Elf Armour", "Light Elf Armour", 2);

        try {
            hero.pickUpShield(elfArmour);
        } catch (NotAValidOperation e) {
            // this will never happen
        }
        hero.pickUpWeapon(electricStorm);

        monster.pickUpWeapon(sword);

        EngageGame game = new EngageGame(hero, monster);
        game.start();
        hero.increaseExperience(10);
        game.charactersInBattle();
        game.newTurn();
        game.charactersInBattle();
    }

}
