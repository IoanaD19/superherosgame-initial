package com.iquestint.superheros;


public class Shield extends GameItem {
    private double protection;

    public Shield(String name, String description, double protection) {
        super(name, description);
        this.protection = protection;
    }

    public double getProtection() {
        return protection;
    }
}
