package com.iquestint.superheros;

import java.text.DecimalFormat;


public class EngageGame {
    private GameCharacter hgc;
    private GameCharacter mgc;

    public EngageGame(GameCharacter hgc, GameCharacter mgc) {
        this.hgc = hgc;
        this.mgc = mgc;
    }

    public void charactersInBattle() {
        // -----------------------------------
        // Get best weapon of Hero + its damage
        // System.out.println("Fight begins");
        Weapon heroW = null;
        int maxDamage = 0;
        // start of for
        for (Weapon w : hgc.weapons) {
            if (w.isActive() && maxDamage < w.getDamage()) {
                maxDamage = w.getDamage();
                heroW = w;
            }
        } // end of for
        int strOfStr = hgc.getAttackDamage(heroW);
        System.out.println("\"" + hgc.getName() + "\"" + " hit \""
                        + mgc.getName() + "\" with " + heroW.getName()
                        + ". Strength of hit: "
                        + new DecimalFormat("#.##").format(strOfStr)
                        + " damage.");
        // -----------------------------------
        // Get best weapon of Monster + its damage
        Weapon monsterW = mgc.getBestWeapon();
        int strOfMonStr = mgc.getAttackDamage(monsterW);

        System.out.println("\"" + mgc.getName() + "\"" + " hit \""
                + hgc.getName() + "\" with " + monsterW.getName()
                + ". Strength of hit: "
                + new DecimalFormat("#.##").format(strOfMonStr)
                + " damage.");
        heroW.useWeapon();
        monsterW.useWeapon();

        // Update health of Hero after receiving a hit!
        updateHealth(hgc, strOfMonStr);
        // Update health of Monster after receiving a hit!
        updateHealth(mgc, strOfStr);

        // update characters
        renderCharacter(hgc);
        renderCharacter(mgc);
    }

    private void updateHealth(GameCharacter character, int strengthOfStrike) {
        int totalProtection = character.getDefenseProtection();
        if (strengthOfStrike > totalProtection) {
            character.currentHealth = character.currentHealth + totalProtection - strengthOfStrike;
        } else {
            // nothing needs to be done
        }
    }

    public void newTurn() {
        hgc.newTurn();
        mgc.newTurn();
        // update characters
        renderCharacter(hgc);
        renderCharacter(mgc);
    }

    public void start() {
        renderStartGame(this);
        renderCharacter(hgc);
        renderCharacter(mgc);
    }

    private void renderStartGame(EngageGame game) {
        System.out.println("Game starts");
    }

    public void end() {
        renderEndGame(this);
    }

    private void renderEndGame(EngageGame game) {
        System.out.println("Game ends");
    }

    public void renderCharacter(GameCharacter character) {
        if (character.isAlive()) {
            System.out.println("\"" + character.getName() + "\": health = " + character.getCurrentHealth());
        } else {
            System.out.println("\"" + character.getName() + "\" is dead.");
        }
    }
}
